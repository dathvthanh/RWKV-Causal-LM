# RWKV Causal LM

A simplify version of [RWKV-INFCTX](https://github.com/PicoCreator/RWKV-LM-LoRA/tree/picocreator-dev-infctx). This repo provide a method to full finetuning the RWKV in CLM type.

## Environment setup
Set up environment for Linux
```bash
sudo apt-get install -y libcusparse-dev-11-2 libcublas-dev-11-2 libcusolver-dev-11-2 libcurand-dev-11-2
sudo apt-get install ninja-build
```
Create new conda environment
```bash
conda update conda

conda create -n rwkv -y python=3.11 pip
conda activate rwkv
```
Set up dependencies
```bash
bash env.sh
```

## Prepare datset
The rwkv_tokenizer does not provided the EOS_TOKEN into the tokenizer file, but according for the pretrained process, the EOS_TOKEN_ID = 0, and I remaping the EOS_TOKEN_ID to token '</s>'. Please add the '</s>' token to end of each sample to activate the EOS, but sometime the rwkv_toknizer does not work correctly if you write any character before or after '</s>'. So you must put the white space in the end character and EOS_TOKEN to tokenize correctly.

For example: 'The Milky way. </s>'

An acceptable format for data file is a json file that each line contain a sample, each sample is a dict, that contains 'prompt' field for the prefix, or the non-masking input_ids, remain field will have name 'completion' field contain the text will be masked or text label for the 'prompt'.
```json
{'prompt': '1+1=', 'completion': '2'}
{'prompt': 'What galaxies that contain solar system?', 'completion': 'The Milky way.'}
```

Transform json data file into `datasets` type, which will contain `input_ids`, `token_type_ids` and `attention_mask` to training and evaluating.
```bash
python preprocess.py --data_path 'dir/to/json/file' \
    --save_dir 'dir/to/output/datasets' \
    --...
```
You can set either `fillter_length` to `True` to fillter out all the samples that have length less than min_length and bigger than max_length.

## Training
```bash 
python3 lightning_trainer.py fit -c {your_config}.yaml
```
The config in `config-3B.yaml` is an examples for training in 4 A100s 40GB in RWKV-World 3B version, for about ~100k sample per epoch, it will take ~4h/epoch.
The `save_dir` in above prepare dataset above will fill into `data_path` in `config.yaml` file and and ensure that `skip_datapath_setup` to be `False` to guarantee not re-process data and occurs unexpected errors.
Other config you can reference for `config-example.yaml` file for suitable for your hardware.

## Services
For other services, like convert checkpoints, or infinite context length set up training, please see details in the [Root Repository](https://github.com/PicoCreator/RWKV-LM-LoRA/tree/picocreator-dev-infctx).