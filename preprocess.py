import argparse
from src.dataflow.trie_tokenizer import TRIE_TOKENIZER
from datasets import load_dataset

def prepare_data_static(args):
    src_dataset = load_dataset("json", data_files=args['data_file'], num_proc=args['num_proc'])
    world_tokenizer = TRIE_TOKENIZER(args['tokenizer_file'])

    # Function used to tokenize the dataset as per HF tokenizer format
    # if given the textual data, it will return the tokenized data
    def encodeTokens(x):
        # If x is an array of strings, we encode them seperately, and conslidate the result
        if isinstance(x, list):
            id_arr = []
            type_arr = []
            mask_arr = []
            for i in range(len(x)):
                enc_str = world_tokenizer.encode(x[i])
                id_arr.append(enc_str)
                type_arr.append([0] * len(enc_str))
                mask_arr.append([1] * len(enc_str))

            # Consolidate the result
            return {
                'input_ids': id_arr,
                'token_type_ids': type_arr,
                'attention_mask': mask_arr
            }

        # Else we encode the string and return it following the HF tokenizer format
        enc_str = world_tokenizer.encode(x)
        return {
            'input_ids': enc_str,
            'token_type_ids': [0] * len(enc_str),
            'attention_mask': [1] * len(enc_str)
        }

    def map_tokenizer(x):
        if 'prompt' in x and 'completion' in x:
            # Array of output values we will return
            input_ids = None
            token_type_ids = None
            attention_mask = None

            # Tokenize both prompt and completion
            # Note that the tokenizer will process and return the input_ids in batches
            prompt_encodings = encodeTokens(x['prompt'])
            completion_encodings = encodeTokens(x['completion'])
            # Join the two input_ids lists
            input_ids = prompt_encodings['input_ids'] + completion_encodings['input_ids']
            # Join the two token_type_ids lists
            token_type_ids = prompt_encodings['token_type_ids'] + completion_encodings['token_type_ids']
            # Setup the attention mask, 0 for prompt, 1 for completion, if masking is enabled
            attention_mask = ([0] * len(prompt_encodings['input_ids']) + [1] * len(completion_encodings['input_ids']))
            # Prepare and return the output object
            return {
                'input_ids': input_ids,
                'token_type_ids': token_type_ids,
                'attention_mask': attention_mask,
            }

        # Fallback to standard text tokenization

    # Map the dataset to the tokenizer, removing the old text column

    src_dataset = src_dataset.map(map_tokenizer, batched=False, num_proc=args['num_proc'])

    # Remove all features, except input_ids, token_type_ids and attention_mask
    # as the metadata/etc columns may cause problems down the line (when passed to the trainer)
    dataset_features = src_dataset["train"].features
    dataset_features_to_remove = {k: v for k, v in dataset_features.items() if k not in ["input_ids", "token_type_ids", "attention_mask"]}
    src_dataset = src_dataset.remove_columns(list(dataset_features_to_remove.keys()))

    # Get the newline token

    # See if rechunking is needed, this is useful mostly for "text" based datasets
    # where we would need to split them into "digestable" context length sizes 
    # used for foundation training
    # ---

    # The rechunking function


    # Perform rechunking if needed for "text" based datasets

    # Remove empty datasets (it causes an error otherwise)
    # and perform min/max length filtering (if configured)
    def dataset_filter(x):
        row_length = len(x["input_ids"])
        if row_length <= 0:
            return False
        if row_length < args['min_length']:
            return False
        if row_length > args['max_length']:
            return False
        return True
    
    if args['fillter_length']:
        src_dataset = src_dataset.filter(dataset_filter)

    # Perform rechunking after filtering, if source is not a "text" based 
    # dataset and text_rechunk_force is enabled

    # Check if the dataset does not have a test split
    # and if so, perform the split
    if 'test' not in src_dataset.keys():
        test_split = args['test_size']
        # The minimum test size is 1, if not we will get errors in the trainer?
        if test_split <= 0 or test_split <= 0.0:
            test_split = 1
        src_dataset = src_dataset['train'].train_test_split(
            test_size=test_split,shuffle=False,
            seed=42 #Fixed seed, to prevent train/test reshuffling between test runs
        )
    # Save the dataset to disk
    src_dataset.save_to_disk(args['save_dir'])
        
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_file', type=str, help='Data file in json format, each line is contains "prompt" and "completion"', require=True)
    parser.add_argument('--tokenizer_file',  type=str, help='Path to world tokenizer file', default="src/dataflow/rwkv_vocab_v2.txt")
    parser.add_argument('--save_dir', type=str, help="Path to save preprocessed datasets.", require=True)
    parser.add_argument('--fillter_length', type=bool, help="Fillter dataset based on lenght input_ids, if True, please provide min and max_length", default=False)
    parser.add_argument('--min_length', type=int, help="Minimum length filltered out dataset", default=512)
    parser.add_argument('--max_length', type=int, help="Maximum length filltered out dataset", default=4096)
    parser.add_argument('--num_proc', type=int, help="Number of process to process mapping function, recommend == number_cpus to prevent bottle necks", default=50)
    parser.add_argument('--test_size', type=float, help="Test size for test set, must be > 0", default=0.01)
    # Parse the args
    args = parser.parse_args()
    
    prepare_data_static(args)
    
if __name__ == "__main__":
    main()